<?php

namespace Fbeen\MyLibrary;

/**
 * Description of MyLibrary
 *
 * @author Frank Beentjes <frankbeen@gmail.com>
 */
class MyLibrary
{
    public function returnSomething()
    {
        return 'Hi Folks';
    }
}
